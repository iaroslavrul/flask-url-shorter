import random
import re
from datetime import date, timedelta, datetime
import sqlite3
from flask import Flask, jsonify, redirect, g, render_template

from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField
from wtforms.validators import DataRequired

app = Flask(__name__)

app.config['SECRET_KEY'] = "Best Secret Key"

# Название файла БД
DATABASE = 'ShortURL.db'


class LinkForm(FlaskForm):
    '''
    Класс - форма для ввода длинной ссылки
    '''
    name = StringField("Введите ссылку, которую хотите сократить", validators=[DataRequired()])
    expire = IntegerField("Введите желаемый livetime ссылки (от 1 дня до 365)")
    submit = SubmitField("Сократить")


# Необходимая функция для работы с БД
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


# Необходимая функция для работы с БД
@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


# Функция, которая выполняет запросы в БД, с возможностью закомитить данные
def query_db(query, args=(), one=False, commit=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    if commit:
        get_db().commit()
    return (rv[0] if rv else None) if one else rv


# Функция для разбития переданной строки на ссылку и livetime (при наличии)
def url_part_trimmer(link):
    work_days_of_link = re.search(r':days:(-)?\d+$', link)
    if work_days_of_link is not None:
        work_days_of_link = int(work_days_of_link.group(0)[6:])
        link = link[:-(len(str(work_days_of_link)) + 6)]
        if work_days_of_link < 1 or work_days_of_link > 365:
            return link, None
    else:
        work_days_of_link = 90
    terminal_day_of_work_link = (date.today() + timedelta(days=int(work_days_of_link))).strftime("%Y-%m-%d")
    return link, terminal_day_of_work_link


# Функция для генерации короткой ссылки
def short_link_generator(link):
    available_symbols = list("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
    i = 0
    short_link = ""
    short_links_list = [y for x in query_db('SELECT link, short_link, expire_date FROM links') for y in x]
    while i < 4:
        short_link += available_symbols[random.randint(0, 61)]
        i += 1
        if i == 4 and short_link not in short_links_list:
            short_links_list.append(short_link)
            return short_link
        elif i == 4 and short_link in short_links_list:
            i = 0


# Обработчик маршута /get-all-links/, который возвращает все обьекты ссылок,которые были сокращены и записаны в базу
@app.route('/get-all-links/', methods=['GET'])
def get_all_links():
    links = []
    full_link = query_db('SELECT link, short_link, expire_date FROM links')
    print(full_link)
    for link_el in full_link:
        links.append({"link": link_el[0], "short_link": link_el[1], "expire_date": link_el[2]})
    return jsonify(links)


# Обработчик маршутов, который обрабатывает запросы, приходящие на директорию /get-short-link/
# и возвращающий короткую ссылку
@app.route('/get-short-link/<path:link>', methods=['GET'])
def get_short_link(link):
    valid = re.match(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', link)
    if valid is None:
        return "Oops, it doesn't look like a link"
    link, terminal_day_of_work_link = url_part_trimmer(link)
    if terminal_day_of_work_link == None:
        return "Please select a expire date in the range from 1 to 365 days or submit the link without ':days:...'"
    all_links = query_db('SELECT link, expire_date, short_link FROM links')
    short_link = list(filter(lambda i: i[0] == link, all_links))
    if len(short_link) == 0:
        short_link = short_link_generator(link)
        query_db('INSERT INTO links(link, expire_date, short_link) VALUES (?, ?, ?)',
                 args=(link, terminal_day_of_work_link, short_link), commit=True)
        return jsonify("http://localhost:5000/{}".format(short_link))
    elif len(short_link) == 1 and short_link[0][1] != terminal_day_of_work_link:
        query_db('UPDATE links SET expire_date=? WHERE short_link=?',
                 args=(terminal_day_of_work_link, short_link[0][2]), commit=True)
    return jsonify("http://localhost:5000/{}".format(short_link[0][2]))


# Страница, на которой пользователь может вводить ссылки и livetime, которые необходимо сократить.
# Валидные ссылки добавляются в БД и выводятся сокращённые ссылки
@app.route('/input/', methods=["GET", "POST"])
def input():
    link_input = LinkForm()
    if link_input.validate_on_submit():
        tmp = link_input.name.data
        expire = link_input.expire.data
        link = tmp + ':days:' + str(expire)
        print('expire: ', expire)
        try:
            res = get_short_link(link).get_data(as_text=True)[1:-2]
            print('link', res)
            if res:
                return render_template('index2.html', url=res)
        except:
            return render_template('index3.html')
    return render_template('index.html', form=link_input)


# Обработчик маршутов, который обрабатывает запросы, приходящие на корневую директорию и перенаправляющий на исходный
# urls, если ранее мы его обработали
@app.route('/<path:short_link>', methods=['GET'])
def short_link_redirect(short_link):
    full_link = query_db('SELECT link, expire_date FROM links WHERE short_link =?', args=(short_link,))
    if len(full_link) != 0:
        if date.today() < datetime.strptime(full_link[0][1], "%Y-%m-%d").date():
            return redirect(full_link[0][0])
        else:
            query_db('DELETE FROM links WHERE short_link=?', args=(short_link,), commit=True)
            return "The short link has expired and has been removed from the database"
    else:
        return "I don't now this link or you have submitted a empty link"


@app.route('/delete/<path:link>', methods=['GET'])
def delete_from_base(link):
    full_link = query_db('SELECT link FROM links WHERE link =?', args=(link,))
    if len(full_link) != 0:
        query_db('DELETE FROM links WHERE link=?', args=(link,), commit=True)
        return "Done! This link has been removed from the database."
    else:
        return "I don't see this link in our database."


def main():
    app.run(port=5000)


if __name__ == '__main__':
    main()
